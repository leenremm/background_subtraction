import cv2
import numpy as np
import datetime
import imutils
import time
import cv2
from skimage.measure import compare_ssim
from scipy import stats

# ==============================================================================

video_filename = 'video\\traffic.mp4'
avg_selector = ["mean", "median", "mode"]
avg_selector_key = 2

max_frame = 200
stack_size = 30
crop_width = 600
crop_height = 600
min_box_size = 20
max_box_size = 500
crop_scale = 50
frame_skip = 5

# ==============================================================================

def rescale_frame(frame, percent=50):
    width = int(frame.shape[1] * percent/ 100)
    height = int(frame.shape[0] * percent/ 100)
    dim = (width, height)
    return cv2.resize(frame, dim, interpolation =cv2.INTER_AREA)

# https://stackoverflow.com/questions/39382412/crop-center-portion-of-a-numpy-image/39382475
def crop_center(img,cropx,cropy):
    y,x,_ = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    return img[starty:starty+cropy,startx:startx+cropx,:]

# ==============================================================================

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture(video_filename)

# Check if camera opened successfully
if (cap.isOpened()== False):
  print("Error opening video stream or file")

ret, frame_old = cap.read()
frame_old = rescale_frame(crop_center(frame_old, crop_width, crop_height), crop_scale)
grey_old = cv2.cvtColor(frame_old, cv2.COLOR_BGR2GRAY)

average_background_array = []
average_background_array.append(grey_old)

ground_truth = cv2.imread("video\\ground_truth.png")
ground_truth = cv2.cvtColor(ground_truth, cv2.COLOR_BGR2GRAY)

frame_i = 1
# Read until video is completed
while(cap.isOpened()):

    # Capture frame-by-frame
    ret, frame_new = cap.read()
    frame_new = rescale_frame(crop_center(frame_new, crop_width, crop_height), crop_scale)

    if frame_i > max_frame:
        break

    if frame_i % frame_skip != 0:
        frame_i += 1
        continue

    if ret == True:

        gray_new = cv2.cvtColor(frame_new, cv2.COLOR_BGR2GRAY)

        # Append image to average
        average_background_array.append(gray_new)
        average_background_stack = np.stack(average_background_array, axis=1)

        # Limit Stack size
        average_background_stack = average_background_stack[:, -stack_size: ,:].copy()
        #print (average_background_stack.shape)

        if (frame_i <= stack_size * frame_skip):
            frame_old = frame_new
            frame_i += 1
            continue

        # Technique
        if (avg_selector[avg_selector_key] == "mean"):
            average_background = np.mean(average_background_stack, axis=1).astype(np.uint8)
        elif (avg_selector[avg_selector_key] == "median"):
            average_background = np.median(average_background_stack, axis=1).astype(np.uint8)
        else:
            average_background = stats.mode(average_background_stack, axis=1)[0].astype(np.uint8).reshape(gray_new.shape)

        # Calculate Error
        error_inverted = cv2.absdiff(ground_truth, average_background)
        error_score = np.sum(error_inverted)/255

        # Foreground
        (score, foreground) = compare_ssim(average_background, gray_new, full=True)
        foreground_inverted = cv2.absdiff(average_background, gray_new)
        foreground = 255 - foreground_inverted

        # Squares
        squares = frame_new.copy()

        # Threshold the difference image, followed by finding contours to
        # obtain the regions of the two input images that differ
        thresh = cv2.threshold(foreground, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

        dilate = cv2.dilate(thresh, None, iterations=1)
        mask = dilate > 0
        dilate_colour = frame_new.copy()
        dilate_colour[mask==False] = 255

        contours = cv2.findContours(dilate.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]
        for c in contours:
            area = cv2.contourArea(c)
            if area > min_box_size and area < max_box_size:
                x,y,w,h = cv2.boundingRect(c)
                cv2.rectangle(squares, (x, y), (x + w, y + h), (36,255,12), 1)
                cv2.rectangle(dilate_colour, (x, y), (x + w, y + h), (36,255,12), 1)

        # Final Image
        show_a1 = cv2.cvtColor(gray_new.copy(), cv2.COLOR_BGR2RGB)
        cv2.putText(show_a1, "Original, Frame %d" % frame_i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)
        show_a2 = cv2.cvtColor(average_background.copy(), cv2.COLOR_BGR2RGB)
        cv2.putText(show_a2, "Average Background, Frame %d" % frame_i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)

        show_b1 = cv2.cvtColor(dilate, cv2.COLOR_BGR2RGB)
        cv2.putText(show_b1, "Threshold + Dilate, Frame %d" % frame_i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)
        show_b2 = dilate_colour
        cv2.putText(show_b2, "Original + Mask, Frame %d" % frame_i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)

        show_c1 = cv2.cvtColor(error_inverted, cv2.COLOR_BGR2RGB)
        cv2.putText(show_c1, "Error, Score: %d, Frame %d" % (error_score, frame_i), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)
        show_c2 = cv2.cvtColor(error_inverted, cv2.COLOR_BGR2RGB)
        cv2.putText(show_c2, "Error, Frame %d" % frame_i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)

        show_d1 = cv2.cvtColor(foreground, cv2.COLOR_BGR2RGB)
        cv2.putText(show_d1, "Difference, Score: %4.3f, Frame %d" % (score, frame_i), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)
        show_d2 = cv2.cvtColor(squares, cv2.COLOR_BGR2RGB)
        cv2.putText(show_d2, "Overlay on Original, Frame %d" % frame_i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)

        final = cv2.hconcat(
            [
                cv2.vconcat([show_a1, show_a2]),
                cv2.vconcat([show_b1, show_b2]),
                cv2.vconcat([show_c1, show_c2]),
                cv2.vconcat([show_d1, show_d2])
            ])

        cv2.imshow('Frame',final)
        print(frame_i, ": %d" % error_score)

        frame_old = frame_new
        frame_i += 1

        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
          break

    # Break the loop
    else:
        break

input("Press any key to save images and end...")

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()

encode_param = [int(cv2.IMWRITE_PNG_COMPRESSION), 0]
prefix = avg_selector[avg_selector_key]
cv2.imwrite('output_img\\%s_average.png' % prefix, average_background, encode_param)
cv2.imwrite('output_img\\%s_foreground.png' % prefix, foreground, encode_param)
cv2.imwrite('output_img\\%s_overlay.png' % prefix, squares, encode_param)
cv2.imwrite('output_img\\%s_error.png' % prefix, error_inverted, encode_param)

print ("Image saved.")
